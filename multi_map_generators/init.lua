local multi_map_generators_path = minetest.get_modpath("multi_map_generators")

multi_map.number_of_layers = 12
multi_map.layers_start_chunk = 500
multi_map.layer_height_chunks = 20
multi_map.wrap_layers = true

dofile(multi_map_generators_path.."/mmgen_levels.lua")
--dofile(multi_map_generators_path.."/mmgen_lvm_example.lua")
dofile(multi_map_generators_path.."/mmgen_simple.lua")
dofile(multi_map_generators_path.."/mmgen_testauri.lua")
dofile(multi_map_generators_path.."/mmgen_mytest.lua")

multi_map.register_fallback_generator("Default Simple", mmgen_simple.generate)

multi_map.register_generator(0, mmgen_testauri.generate,{biomegen=true})
multi_map.register_generator(1, mmgen_mytest.generate, {biomegen=true})
multi_map.register_generator(2, mmgen_simple.generate, {nodetype="mcl_core:stone",biomegen=true})
multi_map.register_generator(3, mmgen_simple.generate, {nodetype="mcl_core:obsidian"})
multi_map.register_generator(4, mmgen_simple.generate, {nodetype="mcl_core:stone"})
multi_map.register_generator(5, mmgen_levels.generate)


multi_map.set_layer_params(0, { name = "testauri" })
multi_map.set_layer_params(1, { name = "testmg"})
multi_map.set_layer_params(2, { name = "Dirt Layer" })
multi_map.set_layer_params(3, { name = "Obby Layer" })
multi_map.set_layer_params(4, { name = "Levels" })


for i=0,multi_map.number_of_layers-1 do
	multi_map.register_linked_layer(i, multi_map.world_edge.POSITIVE_X, i+1,true)
	multi_map.register_linked_layer(i, multi_map.world_edge.NEGATIVE_X, i-1,true)
end

minetest.register_chatcommand("mmtp",{privs={debug=true},func=function(name,param)
	local ch = 80
	local p=minetest.get_player_by_name(name)
	local lp=p:get_pos()
	local ty = ( multi_map.layers_start_chunk * ch ) + (tonumber(param) * multi_map.layer_height_chunks * ch) - 30912 + (10 * ch)
	p:set_pos(vector.new(lp.x,ty,lp.z))
	return true, "tped to layer "..param
end})

-- 3D noise

multi_map.register_global_3dmap(
	"terrain", {
		offset = 0,
		scale = 1,
		spread = {x=384, y=192, z=384},
		seed = 5900033,
		octaves = 5,
		persist = 0.63,
		lacunarity = 2.0,
	}
)

-- 2D noise

multi_map.register_global_2dmap(
	"spike", {
		offset = 0,
		scale = 1,
		spread = {x=128, y=128, z=128},
		seed = -188900,
		octaves = 3,
		persist = 0.5,
		lacunarity = 2.0,
		flags = "noeased"
	}
)
