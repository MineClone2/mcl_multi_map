mmgen_mytest = {}

function mmgen_mytest.generate(current_layer, vm, a, data, minp, maxp, offset_minp, offset_maxp,arg,seed)
	local t1 = os.clock()

	local c_stone = minetest.get_content_id("mcl_core:stone")
	local c_water = minetest.get_content_id("mcl_core:water_source")

	local sidelen = maxp.x - minp.x + 1

	local nvals=multi_map.get_global_3dmap_flat("terrain", {x=sidelen, y=sidelen, z=sidelen}, {x=minp.x, y=minp.y, z=minp.z})

	local ni = 1
	for z = minp.z, maxp.z do
	for y = minp.y, maxp.y do
	for x = minp.x, maxp.x do
	local yy=multi_map.get_offset_y(y)
		if nvals[ni] - (yy - 25) / 55 > 0.5 then
			local vi = a:index(x, y, z)
			if yy < 1 then
				data[vi] = c_stone
			else
				data[vi] = c_stone
			end
		elseif yy < 1 then
			local vi = a:index(x, y, z)
			data[vi] = c_water
		end
		ni = ni + 1
	end
	end
	end
end
